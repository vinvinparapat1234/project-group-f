#include<bits/stdc++.h>
#include<stdio.h>
#include<string.h>
#include<conio.h>
#include<stdlib.h>
#include<ctype.h>
#include"24.cpp"
#include"typing.cpp"

void mainMenu();
void helpChoice();
void quizGuide();
void typingGuide();
void duaEmpatGuide();
void choiceGame();
void main_24();
void firstPage();
void exitAnimation();
char s='\\';
int main(){
	firstPage();
	return 0;
}

void firstPage(){
	system("cls");
	printf("   __          __  _                            _______\n");
	printf("   %c %c        / / | |                          |__   __|                \n", s,s);
	printf("    %c %c  /%c  / /__| | ___ ___  _ __ ___   ___     | | ___               \n",s,s,s);
	printf("     %c %c/  %c/ / _ %c |/ __/ _ %c| '_ ` _ %c / _ %c    | |/ _ %c              \n",s,s,s,s,s,s,s,s);
	printf("      %c  /%c  /  __/ | (_| (_) | | | | | |  __/    | | (_) |             \n",s,s);
	printf(" __ _ _%c/  %c/ %c___|_|%c___%c___/|_| |_| |_|%c___|    |_|%c___/              \n",s,s,s,s,s,s,s);
	printf("|  ____|         | |          | | (_)                                   \n");
	printf("| |__ _   _ _ __ | |_ __ _ ___| |_ _  ___    __ _  __ _ _ __ ___   ___  \n");
	printf("|  __| | | | '_ %c| __/ _` / __| __| |/ __|  / _` |/ _` | '_ ` _ %c / _ %c \n",s,s,s);
	printf("| |  | |_| | | | | || (_| %c__ %c |_| | (__  | (_| | (_| | | | | | |  __/ \n",s,s);
	printf("|_|   \\__,_|_| |_|%c__%c__,_|___/%c__|_|%c___|  %c__, |%c__,_|_| |_| |_|%c___| \n",s,s,s,s,s,s,s,s,s);
	printf("                                             __/ |                      \n");
	printf("                                            |___/                       \n");
	printf("________________________________________________________________________\n");
	printf("________________________________________________________________________\n");
	printf(" ===> Tekan sembarang  untuk menuju ke menu utama \n");
	printf("________________________________________________________________________\n");
	printf("________________________________________________________________________\n");
	if(toupper(getch())){
		mainMenu();
	}
}


void mainMenu(){
	system("cls");
	printf("___________________________________________________\n");
	printf("                    WELCOME TO                     \n");
	printf("                  FUNTASTIC GAME                   \n");
	printf("___________________________________________________\n");
	printf("___________________________________________________\n");
	printf(" ===> Tekan S untuk memilih  permainan\n");
	printf(" ===> Tekan H untuk melihat paduan permainan\n");
	printf(" ===> Tekan Q untuk keluar dari permainan\n");
	printf("___________________________________________________\n");
	printf("___________________________________________________\n");
	char choice;
	choice=toupper(getch());
	if(choice=='Q'){
		exitAnimation();
	}
	else if(choice=='H'){
		helpChoice();
	}
	else if(choice=='S'){
		choiceGame();
	}
}

void helpChoice(){
	system("cls");
	printf("___________________________________________________\n");
	printf("                ~~~BUKU PADUAN~~~                  \n");
	printf("___________________________________________________\n");
	printf("___________________________________________________\n");
	printf("      Game ini akan dibagi menjadi 3 bagian\n");
	printf("1. Quiz Game\n");
	printf("2. Typing Game\n");
	printf("3. 24 Game\n");
	printf("___________________________________________________\n");
	printf("___________________________________________________\n");
	printf(" ===> Tekan 1 untuk melihat paduan Quiz Game\n");
	printf(" ===> Tekan 2 untuk melihat paduan Typing Game\n");
	printf(" ===> Tekan 3 untuk melihat paduan 24 Game\n");
	printf(" ===> Tekan Q untuk kembali ke menu utama\n");
	printf("___________________________________________________\n");
	printf("___________________________________________________\n");
	char choice;
	choice=toupper(getch());
	if(choice=='Q'){
		mainMenu();
	}
	else if(choice=='1'){
		quizGuide();
	}
	else if(choice=='2'){
		typingGuide();
	}
	else if(choice=='3'){
		duaEmpatGuide();
	}
}

void quizGuide(){
	system("cls");
	printf("___________________________________________________\n");
	printf("                   BUKU PANDUAN                    \n");
	printf("                    QUIZ GAME                      \n");
	printf("___________________________________________________\n");
	printf("___________________________________________________\n");
	printf("Didalam game ini player akan diberikan quiz tentang\n");
	printf("berbagai hal. Baik itu meliputi ilmu pengetahuan   \n");
	printf("umum, matematika, logika dan lain-lain.\n");
	printf("\n");
	printf("Adapun soal-soal yang diberikan adalah 5 tingkat   \n");
	printf("awal sebagai pemanasan dan 10 soal tingkat lanjutan\n");
	printf("\n");
	printf("Apabila player berhasil menjawab minimal 3 soal	   \n");
	printf("dari soal tingkat awal. Maka player berhak lanjut  \n");
	printf("menjawab soal tingkat lanjutan\n\n");
	printf("___________________________________________________\n");
	printf(" ===> Tekan sembarang  untuk kembali ke menu utama \n");
	printf("___________________________________________________\n");
	if(toupper(getch())){
		mainMenu();
	}
}

void typingGuide(){
	system("cls");
	printf("___________________________________________________\n");
	printf("                   BUKU PANDUAN                    \n");
	printf("                    TYPING GAME                    \n");
	printf("___________________________________________________\n");
	printf("___________________________________________________\n");
	printf("Didalam game ini player ditugaskan untuk mengetik  \n");
	printf("setiap kata-kata yang muncul. Adapun cara bermain  \n");
	printf("adalah player akan diberikan kata acak. Dimana kata\n");
	printf("tersebut terdiri dari 3-10 huruf.\n");
	printf("\n");
	printf("Tugas dari player adalah mengetik kata yang muncul.\n");
	printf("Apabila sudah selesai mengetik. Silahkan menekan   \n");
	printf("tombol enter untuk mengirim jawaban yang sudah     \n");
	printf("diketikkan. 									   \n");
	printf("\n");
	printf("___________________________________________________\n");
	printf(" ===> Tekan sembarang  untuk kembali ke menu utama \n");
	printf("___________________________________________________\n");
	if(toupper(getch())){
		mainMenu();
	}
}

void duaEmpatGuide(){
	system("cls");
	printf("___________________________________________________\n");
	printf("                   BUKU PANDUAN                    \n");
	printf("                        24                         \n");
	printf("___________________________________________________\n");
	printf("___________________________________________________\n");
	printf("Didalam game ini player akan diberikan 4 angka     \n");
	printf("random. Tugas player adalah mengoperasikan angka   \n");
	printf("tersebut agar dapat menghasilkan angka 24.\n");
	printf("\n");
	printf("Dalam hal ini player boleh menggunakan operasi     \n");
	printf("pertambahan, pengurangan, perkalian dan pembagian. \n");
	printf("Beberapa simbol yang digunakan adalah sebagai      \n");
	printf("berikut (+) sebagai simbol penjumlahan; (-) sebagai\n");
	printf("simbol pengurangan; (x) huruf kecil x sebagai	   \n");
	printf("simbol perkalian; (:) sebagai simbol pembagian	   \n");
	printf("\n");
	printf("___________________________________________________\n");
	printf(" ===> Tekan sembarang  untuk kembali ke menu utama \n");
	printf("___________________________________________________\n");
	if(toupper(getch())){
		mainMenu();
	}
}

void choiceGame(){
	system("cls");
	printf("___________________________________________________\n");
	printf("               SILAHKAN MEMILIH PERMAINAN          \n");
	printf("                   YANG AKAN DIMAINKAN             \n");
	printf("___________________________________________________\n");
	printf("___________________________________________________\n");
	printf(" ===> Tekan 1 untuk memilih Quiz Game\n");
	printf(" ===> Tekan 2 untuk memilih Typing Game\n");
	printf(" ===> Tekan 3 untuk memilih 24 Game\n");
	printf(" ===> Tekan Q untuk kembali ke menu utama\n");
	printf("___________________________________________________\n");
	printf("___________________________________________________\n");
	char choice;
	choice=toupper(getch());
	if(choice=='Q'){
		mainMenu();
	}
	else if(choice=='3'){
		main_24();
	}
	else if(choice=='2'){
		main_Typing();
	}
}

void exitAnimation(){
	system("cls");
	printf("                                    )                \n");
	printf("  *   )                          ( /(             )  \n");
	printf("` )  /(  (  (  (     )      )    )\\())  )   (  ( /(  \n");
	printf(" ( )(_))))\\ )( )\\   (    ( /(  |((_)\\( /( ( )\\ )\\()) \n");
	printf("(_(_())/((_|()((_)  )\\  ')(_)) |_ ((_)(_)))((_|(_)\\  \n");
	printf("|_   _(_))  ((_|_)_((_))((_)_  | |/ ((_)_((_|_) |(_) \n");
	printf("  | | / -_)| '_| | '  \\() _` | | ' </ _` (_-< | ' \\  \n");
	printf("  |_| \\___||_| |_|_|_|_|\\__,_| |_|\\_\\__,_/__/_|_||_| \n");
	exit(1);
}
